#!/bin/bash

export TOKEN=$( head -c 30 /dev/urandom | xxd -p ) &&
docker run --net=host -d -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --name=proxy jupyter/configurable-http-proxy --default-target http://127.0.0.1:9999 &&
docker run -d \
    --net=host \
    -e CONFIGPROXY_AUTH_TOKEN=$TOKEN \
    -v /var/run/docker.sock:/docker.sock \
    jupyter/tmpnb \
    python orchestrate.py --image='jupyter/scipy-notebook' \
        --command='start-notebook.sh \
            "--NotebookApp.base_url={base_path} \
            --NotebookApp.port_retries=0 \
            --ip=0.0.0.0 \
            --NotebookApp.token=""\
            --port={port} \
            --NotebookApp.trust_xheaders=True"'
